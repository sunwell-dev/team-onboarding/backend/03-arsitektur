# Arsitektur Perangkat Lunak
Selain daftar architectural-style yang diberikan di sini, arsitektur juga bermakna "any important design decisions" yang perlu diketahui oleh setiap programmer dalam tim.

## Aristektur yang Perlu Dikenal
Meliputi:
1. Layered Architecture
2. Onion Architecture
3. Clean Architecture
4. Hexagonal Architecture

## Arsitektur pada Nexus
Aristektur yang digunakan adalah campuran antara
1. Layered
2. Hexagonal architecture

## Bacaan Lanjut
1. "Coding an Architecture Style", Enrique Pablo Molinari, Leanpub, 2020
2. "Sustainable Software Architecture - Analyze and Reduce Technical Debt", Carola Lilienthal, dpunkt.verlag, 2019
3. "Get Your Hands Dirty on Clean Architecture" 2nd edition, Tom Hombergs, Leanpub, 2023
4. [Hexagonal Architecture - What Is It? Why Should You Use It?](https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture/), Sven Woltmann, 2023
5. [Hexagonal Architecture with Java – Tutorial](https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture-java/), Sven Woltmann, 2023
6. [Hexagonal Architecture With Spring Boot - Tutorial](https://www.happycoders.eu/software-craftsmanship/hexagonal-architecture-spring-boot/), Sven Woltmann, 2023

Untuk buku silakan ditanya, mungkin ada yang terdapat dalam koleksi Sunwell.